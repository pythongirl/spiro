use bevy::{prelude::*, window::PrimaryWindow};
use bevy_egui::{egui, EguiContexts, EguiPlugin, EguiSettings};
use bevy_fundsp::prelude::*;

mod audio;

#[derive(Clone, Resource)]
struct CurveParameters {
    amp: Shared<f32>,
    f: Shared<f32>,
    k1: Shared<f32>,
    k2: Shared<f32>,
    p: Shared<f32>,
    h: Shared<f32>,
}

// const PI: f64 = std::f64::consts::PI;

fn make_epiepitrochoid_net(par: &CurveParameters) -> impl Clone + 'static + Fn() -> Net32 {
    let recip = || map(|t: &Frame<f32, U1>| 1.0 / t[0]);

    let amp = var(&par.amp);
    let f = var(&par.f);
    let k1 = var(&par.k1);
    let k2 = var(&par.k2);
    let p = var(&par.p);
    let h = var(&par.h);

    let r1 = constant(1.0);
    let r2 = k1.clone() >> recip();
    let r3 = k2.clone() >> recip();
    let h = h.clone();

    let rp = p.clone() >> recip();

    let scale = (r1.clone() + 2.0 * r2.clone() + r3.clone() + h.clone()) >> recip();

    let c1_f = f.clone();
    let c2_f = f.clone() * (1.0 + r1.clone() * k1.clone() - r1.clone() * k1.clone() * rp.clone());

    let c3_f = f.clone()
        * (1.0 + r1.clone() * k1.clone()
            - r1.clone() * k1.clone() * rp.clone()
            - r1.clone() * k2.clone() * rp.clone());

    let unit = (c1_f >> audio::circle()) * ((r1.clone() + r2.clone()) >> split::<U2>())
        + (c2_f >> audio::circle()) * ((r2.clone() + r3.clone()) >> split::<U2>())
        + (c3_f >> audio::circle()) * (h >> split::<U2>());

    let volume_unit = unit * (amp.clone() * scale >> split::<U2>());
    let net = Net32::wrap(Box::new(volume_unit));

    move || net.clone()
}

fn make_epitrochoid_net(parameters: &CurveParameters) -> impl Clone + 'static + Fn() -> Net32 {
    let k = var(&parameters.k1);
    let r2 = k.clone() >> map(|k| 1.0 / k[0]);
    let h = var(&parameters.h);
    let f = var(&parameters.f);

    let unit = (f.clone() >> audio::circle()) * ((r2 + 1.0) >> split::<U2>())
        + ((f * (k + 1.0)) >> audio::circle()) * (h >> split::<U2>());

    let volume_unit = unit * (var(&parameters.amp) >> split::<U2>());

    let net = Net32::wrap(Box::new(volume_unit));

    move || net.clone()
}

#[derive(Copy, Clone, Resource)]
struct GraphId(bevy::utils::Uuid);

fn main() {
    let curve_parameters = CurveParameters {
        amp: shared(0.1),
        f: shared(440.0),
        k1: shared(3.0 / 5.0),
        k2: shared(2.5),
        p: shared(1.0),
        h: shared(0.5),
    };

    let epitrochoid_graph = make_epiepitrochoid_net(&curve_parameters);

    let epitrochoid_graph_id = epitrochoid_graph.id();

    let play_noise = move |mut assets: ResMut<Assets<DspSource>>,
                           dsp_manager: Res<DspManager>,
                           mut audio: ResMut<Audio<DspSource>>| {
        let source = dsp_manager
            .get_graph_by_id(&epitrochoid_graph_id)
            .expect("DSP source not found");

        audio.play_dsp(assets.as_mut(), source);
    };

    // let w = Wave32::render(44100.0, 10.0, &mut white_noise());
    // w.save_wav16("test.wav").expect("Could not save wav");

    App::new()
        .insert_resource(ClearColor(Color::rgb(0.0, 0.0, 0.0)))
        .insert_resource(Msaa::Sample4)
        .insert_resource(curve_parameters)
        .insert_resource(GraphId(epitrochoid_graph_id))
        .init_resource::<UiState>()
        .add_plugins(DefaultPlugins)
        .add_plugin(EguiPlugin)
        .add_plugin(DspPlugin::default())
        .add_startup_system(configure_visuals_system)
        .add_system(update_ui_scale_factor_system)
        .add_system(ui_example_system)
        .add_dsp_source(epitrochoid_graph, SourceType::Dynamic)
        .add_startup_system(play_noise.in_base_set(StartupSet::PostStartup))
        .run();
}

// fn play_noise(
//     mut assets: ResMut<Assets<DspSource>>,
//     dsp_manager: Res<DspManager>,
//     mut audio: ResMut<Audio<DspSource>>,
// ) {
//     let source = dsp_manager
//         .get_graph(white_noise)
//         .expect("DSP source not found");

//     audio.play_dsp(assets.as_mut(), source);
// }

#[derive(Default, Resource)]
struct UiState {
    painting: Oscilloscope,
}

fn configure_visuals_system(mut contexts: EguiContexts) {
    contexts.ctx_mut().set_visuals(egui::Visuals {
        window_rounding: 5.0.into(),
        ..Default::default()
    })
}

fn update_ui_scale_factor_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut toggle_scale_factor: Local<Option<bool>>,
    mut egui_settings: ResMut<EguiSettings>,
    windows: Query<&Window, With<PrimaryWindow>>,
) {
    if keyboard_input.just_pressed(KeyCode::Slash) || toggle_scale_factor.is_none() {
        *toggle_scale_factor = Some(!toggle_scale_factor.unwrap_or(false));

        if let Ok(window) = windows.get_single() {
            let scale_factor = if toggle_scale_factor.unwrap() {
                1.0
            } else {
                1.0 / window.scale_factor()
            };

            egui_settings.scale_factor = scale_factor;
        }
    }
}

fn ui_example_system(
    mut ui_state: ResMut<UiState>,
    mut contexts: EguiContexts,
    dsp_manager: Res<DspManager>,
    graph_id: Res<GraphId>,
    curve_parameters: Res<CurveParameters>,
) {
    let ctx = contexts.ctx_mut();

    egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
        // Top panel
        egui::menu::bar(ui, |ui| {
            egui::menu::menu_button(ui, "File", |ui| {
                if ui.button("Quit").clicked() {
                    std::process::exit(0);
                }
            });
        });
    });

    egui::CentralPanel::default().show(ctx, |ui| {
        ui.heading("Epiepitrochoid Audio Generator & Oscilloscope");
        ui.label(
            "Inspired by this video I saw online and oscilliscope music by Jerobeam Fenderson.",
        );
        ui.hyperlink("https://www.youtube.com/watch?v=n-e9C8g5x68");

        ui.separator();

        ui.heading("Curve Parameters");

        let mut amp = curve_parameters.amp.value();
        let mut f = curve_parameters.f.value();
        let mut k1 = curve_parameters.k1.value();
        let mut k2 = curve_parameters.k2.value();
        let mut p = curve_parameters.p.value();
        let mut h = curve_parameters.h.value();
        ui.add(egui::Slider::new(&mut amp, 0.0..=1.0).text("Amplitude"));
        ui.add(
            egui::Slider::new(&mut f, 100.0..=10000.0)
                .text("Frequency")
                .logarithmic(true),
        );
        ui.add(
            egui::Slider::new(&mut k1, -10.0..=10.0)
                .text("K1")
                .clamp_to_range(false),
        );
        ui.add(
            egui::Slider::new(&mut k2, -10.0..=10.0)
                .text("K2")
                .clamp_to_range(false),
        );
        ui.add(
            egui::Slider::new(&mut p, -10.0..=10.0)
                .text("P")
                .clamp_to_range(false),
        );
        ui.add(
            egui::Slider::new(&mut h, -2.0..=2.0)
                .text("H")
                .clamp_to_range(false),
        );
        curve_parameters.amp.set(amp);
        curve_parameters.f.set(f);
        curve_parameters.k1.set(k1);
        curve_parameters.k2.set(k2);
        curve_parameters.p.set(p);
        curve_parameters.h.set(h);

        ui.separator();
        ui.heading("Oscilloscope");

        ui_state.painting.ui_control(ui);
        egui::Frame::dark_canvas(ui.style()).show(ui, |ui| {
            ui_state.painting.ui_content(ui, dsp_manager, graph_id.0);
        });

        ui.with_layout(egui::Layout::bottom_up(egui::Align::Center), |ui| {
            egui::warn_if_debug_build(ui);
        });
    });
}

struct Oscilloscope {
    gain: f32,
    samples: usize,
    stroke: egui::Stroke,
}

impl Default for Oscilloscope {
    fn default() -> Self {
        Self {
            gain: 9.0,
            samples: 735,
            stroke: egui::Stroke::new(1.0, egui::Color32::LIGHT_GREEN),
        }
    }
}

impl Oscilloscope {
    pub fn ui_control(&mut self, ui: &mut egui::Ui) -> egui::Response {
        ui.horizontal(|ui| {
            egui::stroke_ui(ui, &mut self.stroke, "Stroke");
            ui.separator();
            ui.add(
                egui::Slider::new(&mut self.gain, 0.1..=20.0)
                    .text("Gain")
                    .logarithmic(true),
            );
            ui.separator();
            ui.add(
                egui::Slider::new(&mut self.samples, 10..=10_000)
                    .text("Samples")
                    .logarithmic(true),
            );
        })
        .response
    }

    pub fn ui_content(
        &mut self,
        ui: &mut egui::Ui,
        dsp_manager: Res<DspManager>,
        graph_id: bevy::utils::Uuid,
    ) {
        let (response, painter) =
            ui.allocate_painter(egui::Vec2::new(400.0, 400.0), egui::Sense::drag());

        let rect = response.rect;

        let source = dsp_manager
            .get_graph_by_id(&graph_id)
            .expect("DSP manager did not return source")
            .clone();

        let samples = source
            .into_iter()
            .take(self.samples)
            .collect::<Vec<[f32; 2]>>();

        if samples.len() >= 2 {
            let points: Vec<egui::Pos2> = samples
                .iter()
                .map(|p| {
                    rect.min
                        + egui::vec2(200.0 * p[0] * self.gain, 200.0 * p[1] * self.gain)
                        + egui::vec2(200.0, 200.0)
                })
                .collect();

            painter.add(egui::Shape::line(points, self.stroke));
        }
    }
}
