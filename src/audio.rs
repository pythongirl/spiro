// use fundsp::hacker::*;
use bevy_fundsp::prelude::*;

fn sine_phase<T>(p: T) -> An<Sine<T>>
where
    T: Real,
{
    An(Sine::with_phase(DEFAULT_SR, Some(p)))
}

// let mut n = (saw_hz(440.0) * constant(1.0)) >> map(|t| (sin(t[0]), cos(t[0])));

// let clock = || zero() >> feedback(add(1.0 / 44100.0));
// let pure_saw = || clock() >> map(|f| f[0] % 1.0) * 2.0 - 1.0;

pub fn circle() -> An<Branch<f32, Sine<f32>, Sine<f32>>> {
    sine_phase::<f32>(0.0) ^ sine_phase::<f32>(0.25)
}

pub fn epitrochoid(f: f32, k: f32, h: f32) -> Net32 {
    let r1 = 1.0;
    let r2 = r1 / k;
    let h = h * r2;
    let unit = (dc(f) >> circle() * (r1 + r2)) + (dc(f * (1.0 + r1 / r2)) >> circle() * h);
    return Net32::wrap(Box::new(unit));
}

pub fn write_audio() {
    println!("Hello, world!");

    // Parameters: frequency
    // Output: sine, cosine
    // let circle = || sine_phase::<f32>(0.0) ^ sine_phase::<f32>(0.25);

    let r1 = 1.0;
    let k = 5.0; //-17.0 / 9.0;
    let r2 = r1 / k;
    let h = 1.0 * r2;

    let f: f32 = 440.0;

    let mut spiral = (dc(f) >> circle() * (r1 + r2)) + (dc(f * (1.0 + r1 / r2)) >> circle() * h);

    let mut wave1 = Wave32::render(44100.0, 10.0, &mut (spiral));
    wave1.normalize();

    // let wave2 = wave1.filter(10.0, &mut (pass() * PI >> map(|t| (sin(t[0]), cos(t[0])))));
    wave1.save_wav16("test.wav").expect("could not save wave.");
}
